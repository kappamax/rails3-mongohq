class Article
  include Mongoid::Document
  field :name, type: String
  field :context, type: String
end
